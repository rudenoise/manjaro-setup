#!/bin/sh
set -e

sudo pacman -Syu \
  vim \
  neovim \
  xclip \
  redshift \
  the_silver_searcher \
  fish \
  tree \
  fzf \
  firefox-developer-edition \
  docker-compose \
  exfat-utils \
  xorg-xinput \
  lzip

sudo pacman -R \
    firefox \
    thunderbird

pamac build shuriken

# set up some docker basics
sudo usermod -aG docker $USER
# sudo systemctl start docker
# docker run hello-world

# change default shell to fish
chsh -s `which fish`

# copy the OCRA font to correct location
sudo cp ./OCRAEXT.TTF /usr/share/fonts/TTF/OCRAEXT.TTF

# copy some settings I like
cp -ruT manjaro-conf $HOME/.config


