# manjaro-setup

## To Do:

- git setup
- editor setup (~/.profile)
- add usb scripts
- migrate ubuntu scripts to sh
- look at wine + vst stuff
- packages
  - reaper?


## External Monitor
```
sudo vim /etc/systemd/logind.conf

#HandleLidSwitch=ignore
#HandleLidSwitchExternalPower=ignore
#HandleLidSwitchDocked=ignore

```

## Jack Env Life Cycle

```
# start-up

wine /usr/bin/lin-vst-server-x.exe.so &

/usr/bin/pasuspender -- jackd \
  -t1000 \
  -dalsa \
  -dhw:Rubix22\
  -r96000 \
  -p256 \
  -n3 \
  --realtime\
  &


/usr/bin/a2jmidid -e &

# shut-down
/usr/bin/killall  a2jmidid
/usr/bin/killall  jackd
/usr/bin/killall  wine
```


## Wine and Airwave

sudo pacman -S wine wine-mono

wine installer.exe

plugins @:
`~/.wine/drive_c/Program\ Files/VSTPlugIns/

https://github.com/psycha0s/airwave
https://www.steinberg.net/en/company/developers.html

Issues:
https://github.com/psycha0s/airwave/issues/92
An archive of an older VST SDK that matches the build script
https://web.archive.org/web/*/https://download.steinberg.net/sdk_downloads/vstsdk369_01_03_2018_build_132.zip
uzip the SDK, cd in, run the:
```
./copy_vst2_to_vst3_sdk.sh
```
https://linuxmusicians.com/viewtopic.php?t=17776

```
cmake -DCMAKE_BUILD_TYPE="Release" -DCMAKE_INSTALL_PREFIX=/opt/airwave -DVSTSDK_PATH=$HOME/Downloads/otherVST/VST_SDK/VST3_SDK/ ..
make
sudo make install
```

```
#run
/opt/airwave/bin/airwave-manager
```

## LinVST X

May be more onerous on the CPU? Works nicely. Inter plugin comms are fine using `LinVst-x`

https://github.com/osxmidi/LinVst-X
https://github.com/osxmidi/LinVst-X/releases

Before starting DAW, start the linvst server
```
wine /usr/bin/lin-vst-server-x.exe.so
```

## Melda Plugins

- need arial.ttf installing (used winetricks but not nesesarry)
- ~/.wine/drive_c/windows/Fonts/

```
unzip LinVst-X-2.7-Manjaro.zip
cd LinVst-X-2.7-Manjaro
sudo cp LinVst-X-2.7-Manjaro/embedded/lin-vst-server* /usr/bin
./convert/linvstxconverttree ~/.wine/drive_c/Program\ Files\/VSTPlugIns/
ls -s ~/.wine/drive_c/Program\ Files\/VSTPlugIns/*.so ~/.vst/
```

