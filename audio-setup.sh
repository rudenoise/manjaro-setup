#!/bin/sh
set -e


# install some essential tools
sudo pacman -Syu \
  realtime-privileges \
  qjackctl \
  cadence \
  pulseaudio-jack \
  wine \
  wine-mono \
  wine-gecko \
  lib32-mpg123

# add a user called "rudenoise" to the "realtime" group
sudo usermod -a -G realtime rudenoise
# print the groups "rudenoise" is a part of
groups rudenoise

# ensure group has realtime priority
cat /etc/security/limits.d/99-realtime-privileges.conf
# sould see
# @realtime - rtprio 99

echo "/usr/bin/paususpender -- jackd -v t1000 -dalsa -dhw:Rubix22 -r96000 -p128 -n2" > ~/.jackdrc


if [ ! -d "./audio_tools/" ] ; then
    echo "audio-tools dir not present, add it first"
    exit 1
fi

# install renoise
tar -xf audio_tools/rns_321_linux_x86_64.tar.gz -C /tmp/
sudo /tmp/rns_321_linux_x86_64/install.sh

# install reaper
tar -xf audio_tools/reaper602_linux_x86_64.tar.xz -C /tmp/
sudo /tmp/reaper_linux_x86_64/install-reaper.sh

# install plugins
mkdir -p $HOME/.vst
mkdir -p $HOME/.vst3

# airwindows
unzip audio_tools/AirWindows.zip -d /tmp/
mv /tmp/NewUpdates/LinuxVST/* $HOME/.vst
# tracktion
tar -xf audio_tools/daw_essentials_collection_1.0.36.tar.gz -C /tmp
mv /tmp/DAW\ Essentials\ Collection/*.so $HOME/.vst
# redux
tar -xf audio_tools/rns_rdx_111_linux.tar.gz -C /tmp
mv /tmp/rns_rdx_111_linux/64bit/* $HOME/.vst
