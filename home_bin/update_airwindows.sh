#!/bin/sh

curl http://www.airwindows.com/wp-content/uploads/NewUpdates.zip --output /tmp/AirWindows.zip

unzip /tmp/AirWindows.zip -d /tmp/

cp /tmp/NewUpdates/LinuxVST/*.so $HOME/.vst/

rm -fr /tmp/AirWindows.zip
rm -fr /tmp/NewUpdates
