#!/bin/sh
set -e

sudo pacman -S vim curl neovim wget

cd $HOME/code
CLONE_PATH=./vimrc
if [ ! -d "$CLONE_PATH" ] ; then
  git clone ssh://git@github.com/rudenoise/vimrc.git
fi

cd vimrc
cp .vimrc $HOME/.vimrc
cd $HOME/code/manjaro-setup

mkdir -p $HOME/.vim/autoload;
mkdir -p $HOME/.vim/plugin;

mkdir -p $HOME/.vim/autoload $HOME/.vim/bundle && \
curl -LSso $HOME/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

CLONE_PATH=$HOME/.vim/bundle/vim-go
if [ ! -d "$CLONE_PATH" ] ; then
    git clone https://github.com/fatih/vim-go.git \
        $CLONE_PATH
fi

CLONE_PATH=$HOME/.vim/bundle/syntastic
if [ ! -d "$CLONE_PATH" ] ; then
    git clone --depth=1 https://github.com/scrooloose/syntastic.git\
        $CLONE_PATH
fi

CLONE_PATH=$HOME/.vim/bundle/swift.vim.git
if [ ! -d "$CLONE_PATH" ] ; then
    git clone https://github.com/keith/swift.vim.git $CLONE_PATH
fi

wget https://raw.githubusercontent.com/kien/rainbow_parentheses.vim/master/autoload/rainbow_parentheses.vim -P $HOME/.vim/autoload/;
wget https://raw.github.com/kien/rainbow_parentheses.vim/master/plugin/rainbow_parentheses.vim -P $HOME/.vim/plugin/;

mkdir -p ${XDG_CONFIG_HOME:=$HOME/.config}
ln -s $HOME/.vim $XDG_CONFIG_HOME/nvim
ln -s $HOME/.vimrc $XDG_CONFIG_HOME/nvim/init.vim

#pip install neovim
