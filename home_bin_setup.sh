#!/bin/sh

mkdir -p $HOME/bin

cp home_bin/* $HOME/bin

echo "export PATH=\"\$PATH:\$HOME/bin\"" >> $HOME/.profile
source $HOME/.profile
